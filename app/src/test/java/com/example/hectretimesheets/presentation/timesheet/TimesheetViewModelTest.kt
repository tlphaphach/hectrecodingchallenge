package com.example.hectretimesheets.presentation.timesheet

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.hectretimesheets.MainDispatcherRule
import com.example.hectretimesheets.common.Resource
import com.example.hectretimesheets.data.remote.dto.JobDto
import com.example.hectretimesheets.domain.model.Job
import com.example.hectretimesheets.domain.use_case.timesheet.GetTimesheetUseCase
import com.example.hectretimesheets.domain.use_case.timesheet.SubmitTimesheetUseCase
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import java.io.InputStream
import java.lang.reflect.Type

@RunWith(JUnit4::class)
class TimesheetViewModelTest {
    private lateinit var viewModel: TimesheetViewModel
    private val getTimesheetUseCase = Mockito.mock(GetTimesheetUseCase::class.java)
    private val submitTimesheetUseCase = Mockito.mock(SubmitTimesheetUseCase::class.java)

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        viewModel = TimesheetViewModel(
            getTimesheetUseCase = this.getTimesheetUseCase,
            submitTimesheetUseCase = this.submitTimesheetUseCase
        )
    }

    @Test
    fun `getTimesheet success return non-empty list of jobs`() {
        Mockito.`when`(getTimesheetUseCase())
            .thenReturn(flow { Resource.Success<List<Job>>(getMockedListJob()) })
        viewModel.getJobs()
        viewModel.state.observeForever {
            assert(!it.jobs.isNullOrEmpty() && it.error.isBlank() && !it.isLoading && it.toast.isBlank())
        }
    }

    @Test
    fun `getTimesheet failed return with error message`() = runBlocking {
        Mockito.`when`(getTimesheetUseCase())
            .thenReturn(flow { Resource.Error<List<Job>>("An unexpected error occurred.") })
        viewModel.getJobs()
        viewModel.state.observeForever {
            assert(it.error == "An unexpected error occurred.")
        }
    }

    @Test
    fun `submitTimesheet success return non-empty list of jobs and toast message`() {
        viewModel.submitTimesheet()
        viewModel.state.observeForever {
            assert(!it.jobs.isNullOrEmpty() && it.error.isBlank() && !it.isLoading && it.toast.isNotBlank())
        }
    }

    @Test
    fun `submitTimesheet failed return with error message`() = runBlocking {
        Mockito.`when`(submitTimesheetUseCase(getMockedListJob()))
            .thenReturn(flow { Resource.Error<List<Job>>("An unexpected error occurred.") })
        viewModel.submitTimesheet()
        viewModel.state.observeForever {
            assert(it.error == "An unexpected error occurred.")
        }
    }

    private fun getMockedListJob(): List<Job> {
        return try {
            val inputStream: InputStream =
                TimesheetViewModelTest::class.java.getResourceAsStream("timesheet.json")!!
            val json = inputStream.bufferedReader().use { it.readText() }
            val listType: Type = object : TypeToken<List<JobDto>>() {}.type
            return Gson().fromJson(json, listType)
        } catch (e: Exception) {
            e.printStackTrace()
            emptyList()
        }
    }
}