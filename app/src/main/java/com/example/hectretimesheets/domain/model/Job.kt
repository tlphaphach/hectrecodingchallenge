package com.example.hectretimesheets.domain.model

data class Job(
    val jobName: String,
    val staffs: List<Staff>
) {
    override fun equals(other: Any?): Boolean {
        return if (other !is Job) {
            false
        } else {
            this.jobName == other.jobName
        }
    }

    fun isContentChanged(other: Any?): Boolean {
        return if (other !is Job) {
            false
        } else {
            this.jobName != other.jobName || this.staffs.containsAll(other.staffs)
        }
    }
}
