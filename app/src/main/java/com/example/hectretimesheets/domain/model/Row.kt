package com.example.hectretimesheets.domain.model

data class Row(
    val rowNumber: Int,
    var trees: Int,
    var totalTrees: Int,
    val coWorkers: List<CoWorker> = emptyList(),
    var isSelected: Boolean = false
) {
    val isHavingCoWorker: Boolean = coWorkers.isNotEmpty()
    val availableTress: Int = totalTrees - coWorkers.sumOf { it.trees }

    override fun equals(other: Any?): Boolean {
        return if (other !is Row) {
            false
        } else {
            this.rowNumber == other.rowNumber
        }
    }

    fun isContentChanged(other: Any?): Boolean {
        return if (other !is Row) {
            true
        } else {
            this.trees != other.trees
                    || this.totalTrees != other.totalTrees
                    || this.isSelected != other.isSelected
                    || this.isHavingCoWorker != other.isHavingCoWorker
        }
    }
}

data class CoWorker(
    val name: String,
    val trees: Int,
)
