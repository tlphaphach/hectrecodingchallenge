package com.example.hectretimesheets.domain.model

data class Staff(
    val staffName: String,
    val staffAvatar: Pair<String, Int>,
    val orchard: String,
    val block: String,
    var rate: Double,
    val availableRows: List<Row>
) {
    override fun equals(other: Any?): Boolean {
        return if (other !is Staff) {
            false
        } else {
            this.staffName == other.staffName
        }
    }

    fun isContentChanged(other: Any?): Boolean {
        return if (other !is Staff) {
            false
        } else {
            this.orchard != other.orchard
                    || this.block != other.block
                    || this.rate != other.rate
                    || !this.availableRows.containsAll(other.availableRows)
        }
    }
}
