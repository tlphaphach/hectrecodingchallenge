package com.example.hectretimesheets.domain.use_case.timesheet

import com.example.hectretimesheets.common.Resource
import com.example.hectretimesheets.data.remote.dto.toJob
import com.example.hectretimesheets.domain.model.Job
import com.example.hectretimesheets.domain.repository.TimesheetRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetTimesheetUseCase @Inject constructor(
    private val repository: TimesheetRepository
) {
    operator fun invoke(): Flow<Resource<List<Job>>> = flow {
        try {
            emit(Resource.Loading<List<Job>>())
            val jobs = repository.getTimesheet().map { it.toJob() }
            emit(Resource.Success<List<Job>>(jobs))
        } catch (e: HttpException) {
            emit(Resource.Error<List<Job>>("An unexpected error occurred."))
        } catch (e: IOException) {
            emit(Resource.Error<List<Job>>("Please check your internet connection."))
        }
    }
}