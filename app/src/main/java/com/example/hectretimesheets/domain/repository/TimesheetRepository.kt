package com.example.hectretimesheets.domain.repository

import com.example.hectretimesheets.data.remote.dto.JobDto
import com.example.hectretimesheets.domain.model.Job

interface TimesheetRepository {
    suspend fun getTimesheet(): List<JobDto>
    suspend fun submitTimesheet(data: List<Job>): String
}