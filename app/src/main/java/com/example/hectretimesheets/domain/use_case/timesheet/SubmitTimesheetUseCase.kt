package com.example.hectretimesheets.domain.use_case.timesheet

import com.example.hectretimesheets.common.Resource
import com.example.hectretimesheets.domain.model.Job
import com.example.hectretimesheets.domain.repository.TimesheetRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class SubmitTimesheetUseCase @Inject constructor(
    private val repository: TimesheetRepository
) {
    operator fun invoke(jobs: List<Job>): Flow<Resource<String>> = flow {
        try {
            emit(Resource.Loading<String>())
            val response = repository.submitTimesheet(jobs)
            emit(Resource.Success<String>(response))
        } catch (e: HttpException) {
            emit(Resource.Error<String>("An unexpected error occurred."))
        } catch (e: IOException) {
            emit(Resource.Error<String>("Please check your internet connection."))
        }
    }
}