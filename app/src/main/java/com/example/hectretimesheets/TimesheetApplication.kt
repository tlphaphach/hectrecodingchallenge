package com.example.hectretimesheets

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TimesheetApplication : Application() {
}