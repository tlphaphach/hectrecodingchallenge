package com.example.hectretimesheets.di

import android.content.Context
import com.example.hectretimesheets.common.Constants
import com.example.hectretimesheets.data.HectreTimesheetApi
import com.example.hectretimesheets.data.remote.dto.JobDto
import com.example.hectretimesheets.data.repository.TimesheetRepositoryImpl
import com.example.hectretimesheets.domain.model.Job
import com.example.hectretimesheets.domain.repository.TimesheetRepository
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.delay
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.InputStream
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideHectreTimesheetApi(): HectreTimesheetApi {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(
                OkHttpClient.Builder()
                    .callTimeout(60, TimeUnit.SECONDS)
                    .build()
            )
            .build()
            .create(HectreTimesheetApi::class.java)

    }

    @Provides
    @Singleton
    @Named("MockAPI")
    fun provideMockApi(@ApplicationContext context: Context): HectreTimesheetApi {
        return object : HectreTimesheetApi {
            override suspend fun getTimesheet(): List<JobDto> {
                delay(1000)
                try {
                    val inputStream: InputStream = context.assets.open("timesheet.json")
                    val json = inputStream.bufferedReader().use { it.readText() }
                    val listType: Type = object : TypeToken<List<JobDto>>() {}.type
                    return Gson().fromJson(json, listType)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                return emptyList()
            }

            override suspend fun submitTimesheet(data: List<Job>): String {
                delay(1000)
                return "Timesheet submitted"
            }
        }

    }

    @Provides
    @Singleton
    fun provideTimesheetRepository(@Named("MockAPI") api: HectreTimesheetApi): TimesheetRepository {
        return TimesheetRepositoryImpl(api)
    }
}