package com.example.hectretimesheets.data

import com.example.hectretimesheets.data.remote.dto.JobDto
import com.example.hectretimesheets.domain.model.Job
import retrofit2.http.GET
import retrofit2.http.PUT

interface HectreTimesheetApi {
    @GET("timesheet")
    suspend fun getTimesheet(): List<JobDto>

    @PUT("submit_timesheet")
    suspend fun submitTimesheet(data: List<Job>): String
}