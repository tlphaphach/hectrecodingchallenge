package com.example.hectretimesheets.data.remote.dto

import com.example.hectretimesheets.domain.model.Job

data class JobDto(
    val jobName: String,
    val staffs: List<StaffDto>
)

fun JobDto.toJob(): Job {
    return Job(
        jobName = this.jobName,
        staffs = this.staffs.map { it.toStaff() }
    )
}