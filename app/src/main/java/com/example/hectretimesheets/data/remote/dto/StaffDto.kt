package com.example.hectretimesheets.data.remote.dto

import com.example.hectretimesheets.common.ColorGenerator
import com.example.hectretimesheets.common.TextGenerator
import com.example.hectretimesheets.domain.model.Staff

data class StaffDto(
    val staffName: String,
    val orchard: String,
    val block: String,
    var rate: Double,
    val availableRows: List<RowDto>
)

fun StaffDto.toStaff(): Staff {
    return Staff(
        staffName = this.staffName,
        orchard = this.orchard,
        block = this.block,
        rate = this.rate,
        availableRows = this.availableRows.map { it.toRow() },
        staffAvatar = Pair(
            TextGenerator.generateAvatarTextByName(this.staffName),
            ColorGenerator.getRandomIntColor()
        )
    )
}
