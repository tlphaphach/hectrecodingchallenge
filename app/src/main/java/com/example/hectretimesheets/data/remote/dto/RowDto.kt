package com.example.hectretimesheets.data.remote.dto

import com.example.hectretimesheets.domain.model.Row

data class RowDto(
    val rowNumber: Int,
    var trees: Int,
    var totalTrees: Int,
    val coWorkers: List<CoWorkerDto> = emptyList()
)

fun RowDto.toRow(): Row {
    return Row(
        rowNumber = this.rowNumber,
        trees = this.trees,
        totalTrees = this.totalTrees,
        coWorkers = this.coWorkers.map { it.toCoWorker() }
    )
}
