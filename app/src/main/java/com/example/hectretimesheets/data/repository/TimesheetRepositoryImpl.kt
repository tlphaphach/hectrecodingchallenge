package com.example.hectretimesheets.data.repository

import com.example.hectretimesheets.data.HectreTimesheetApi
import com.example.hectretimesheets.data.remote.dto.JobDto
import com.example.hectretimesheets.domain.model.Job
import com.example.hectretimesheets.domain.repository.TimesheetRepository
import javax.inject.Inject

class TimesheetRepositoryImpl @Inject constructor(
    private val api: HectreTimesheetApi
) : TimesheetRepository {

    override suspend fun getTimesheet(): List<JobDto> {
        return api.getTimesheet()
    }

    override suspend fun submitTimesheet(data: List<Job>): String {
        return api.submitTimesheet(data)
    }

}