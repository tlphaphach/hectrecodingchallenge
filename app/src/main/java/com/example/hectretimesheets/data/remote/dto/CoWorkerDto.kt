package com.example.hectretimesheets.data.remote.dto

import com.example.hectretimesheets.domain.model.CoWorker

data class CoWorkerDto(
    val name: String,
    val trees: Int
)

fun CoWorkerDto.toCoWorker(): CoWorker {
    return CoWorker(
        name = this.name,
        trees = this.trees
    )
}