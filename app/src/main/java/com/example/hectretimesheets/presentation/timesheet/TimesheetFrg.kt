package com.example.hectretimesheets.presentation.timesheet

import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.hectretimesheets.databinding.FrgTimesheetsBinding
import com.example.hectretimesheets.domain.model.Job
import com.example.hectretimesheets.domain.model.Row
import com.example.hectretimesheets.domain.model.Staff
import com.example.hectretimesheets.presentation.BaseFragment
import com.example.hectretimesheets.presentation.timesheet.adapter.JobAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class TimesheetFrg : BaseFragment<FrgTimesheetsBinding>(FrgTimesheetsBinding::inflate),
    TimesheetActionCallback {
    private val viewModel: TimesheetViewModel by viewModels()
    private val jobAdapter = JobAdapter(this)

    override fun observeLiveData() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            if (state.error.isNotEmpty()) {
                showDialog(state.error)
            }

            if (!state.jobs.isNullOrEmpty()) {
                jobAdapter.submitList(state.jobs.toMutableList())
                binding.svMainContent.visibility = View.VISIBLE
            }

            binding.clProgressLoading.root.visibility =
                if (state.isLoading) View.VISIBLE else View.GONE

            if (state.toast.isNotEmpty()) {
                Toast.makeText(requireContext(), state.toast, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun initViews() {
        binding.rvJobs.adapter = jobAdapter
        (binding.rvJobs.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        binding.btnConfirm.setOnClickListener {
            lifecycleScope.launch {
                viewModel.submitTimesheet()
            }
        }
    }

    override fun onApplyToAllClicked(job: Job, rate: Double) {
        viewModel.applyRateToJobStaff(job, rate)
    }

    override fun onTreesPerRowChanged(job: Job, staff: Staff, row: Row, trees: Int) {
        viewModel.updateStaffTreesPerRow(job, staff, row, trees)
    }

    override fun onAddMaxTreesClicked(job: Job) {
        viewModel.addMaxTreesForStaff(job)
    }

    override fun onRowClicked(job: Job, staff: Staff, row: Row, isSelected: Boolean) {
        viewModel.updateRowState(job, staff, row, isSelected)
    }

    override fun onStaffRateChanged(job: Job, staff: Staff, rate: Double) {
        viewModel.updateSingleStaffRate(job, staff, rate)
    }
}