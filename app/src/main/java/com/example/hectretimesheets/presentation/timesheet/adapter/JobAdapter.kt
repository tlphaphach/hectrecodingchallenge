package com.example.hectretimesheets.presentation.timesheet.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.hectretimesheets.databinding.ItemJobBinding
import com.example.hectretimesheets.domain.model.Job
import com.example.hectretimesheets.presentation.timesheet.TimesheetActionCallback

class JobAdapter(
    private var callback: TimesheetActionCallback
) : ListAdapter<Job, JobAdapter.JobVH>(JobDiffCallback()) {

    inner class JobVH(private val binding: ItemJobBinding) : RecyclerView.ViewHolder(binding.root) {
        private val staffAdapter = StaffAdapter()

        fun bindData(data: Job) {
            binding.tvJobTitle.text = data.jobName
            binding.btnAddMaxTree.setOnClickListener {
                callback.onAddMaxTreesClicked(data)
            }
            binding.rvStaffs.setRecycledViewPool(viewPool)
            binding.rvStaffs.adapter = staffAdapter
            (binding.rvStaffs.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            staffAdapter.setJobName(data.jobName)
            staffAdapter.setOnApplyToAllListener { rate ->
                callback.onApplyToAllClicked(data, rate)
            }
            staffAdapter.setOnTreesPerRowChangedListener { staff, row, trees ->
                callback.onTreesPerRowChanged(data, staff, row, trees)
            }
            staffAdapter.setonRowClickedListener { staff, isSelected, row ->
                callback.onRowClicked(data, staff, row, isSelected)
            }
            staffAdapter.setOnStaffRateChangedListener { staff, rate ->
                callback.onStaffRateChanged(data, staff, rate)
            }
            staffAdapter.submitList(data.staffs)
        }
    }

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobVH {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemJobBinding.inflate(inflater, parent, false)
        return JobVH(binding)
    }

    override fun onBindViewHolder(holder: JobVH, position: Int) {
        val data = getItem(position)
        holder.bindData(data)
    }
}

class JobDiffCallback : DiffUtil.ItemCallback<Job>() {
    override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
        return false
    }
}