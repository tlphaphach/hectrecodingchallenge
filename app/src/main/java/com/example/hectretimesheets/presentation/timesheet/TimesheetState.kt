package com.example.hectretimesheets.presentation.timesheet

import com.example.hectretimesheets.domain.model.Job

data class TimesheetState(
    val isLoading: Boolean = false,
    val jobs: List<Job> = emptyList(),
    val error: String = "",
    val toast: String = "",
)