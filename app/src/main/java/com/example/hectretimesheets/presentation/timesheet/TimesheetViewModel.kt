package com.example.hectretimesheets.presentation.timesheet

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hectretimesheets.common.Resource
import com.example.hectretimesheets.domain.model.Job
import com.example.hectretimesheets.domain.model.Row
import com.example.hectretimesheets.domain.model.Staff
import com.example.hectretimesheets.domain.use_case.timesheet.GetTimesheetUseCase
import com.example.hectretimesheets.domain.use_case.timesheet.SubmitTimesheetUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class TimesheetViewModel @Inject constructor(
    private val getTimesheetUseCase: GetTimesheetUseCase,
    private val submitTimesheetUseCase: SubmitTimesheetUseCase
) : ViewModel() {
    private val _state: MutableLiveData<TimesheetState> = MutableLiveData()
    val state: LiveData<TimesheetState> get() = _state

    init {
        getJobs()
    }

    fun getJobs() {
        getTimesheetUseCase().onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = TimesheetState(jobs = result.data ?: emptyList())
                }

                is Resource.Error -> {
                    _state.value =
                        TimesheetState(error = result.message ?: "An unexpected error occurred")
                }

                is Resource.Loading -> {
                    _state.value = TimesheetState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }

    fun submitTimesheet() {
        state.value?.jobs?.let { jobs ->
            submitTimesheetUseCase(jobs).onEach { result ->
                when (result) {
                    is Resource.Success -> {
                        _state.value = TimesheetState(toast = result.data!!)
                    }

                    is Resource.Error -> {
                        _state.value =
                            TimesheetState(error = result.message ?: "An unexpected error occurred")
                    }

                    is Resource.Loading -> {
                        _state.value = TimesheetState(isLoading = true)
                    }
                }
            }.launchIn(viewModelScope)
        }
    }

    fun applyRateToJobStaff(job: Job, rate: Double) {
        val currentData: List<Job> = _state.value?.jobs!!
        currentData.find { it == job }?.let {
            it.staffs.forEach { staff ->
                staff.rate = rate
            }
        }

        _state.postValue(TimesheetState(jobs = currentData))
    }

    fun updateRowState(job: Job, staff: Staff, row: Row, isSelected: Boolean) {
        val currentData: List<Job> = _state.value?.jobs!!
        currentData.find { jobItem ->
            jobItem == job
        }?.let { job ->
            job.staffs.find { staffItem ->
                staffItem == staff
            }?.let { staff ->
                staff.availableRows.find { rowItem ->
                    rowItem == row
                }?.let { row ->
                    row.isSelected = isSelected
                }
            }
        }
        _state.postValue(TimesheetState(jobs = currentData))
    }

    fun updateStaffTreesPerRow(job: Job, staff: Staff, row: Row, trees: Int) {
        val currentData: List<Job> = _state.value?.jobs!!
        val isValidInput = isInputtedTreesValid(job, staff, row, trees)

        currentData.find { jobItem ->
            jobItem == job
        }?.let { job ->
            job.staffs.find { staffItem ->
                staffItem == staff
            }?.let { staff ->
                staff.availableRows.find { rowItem ->
                    rowItem == row
                }?.let { row ->
                    row.trees = if (isValidInput) trees else 0
                }
            }
        }

        if (!isValidInput) {
            _state.postValue(
                TimesheetState(
                    error = "Please input a valid trees number",
                    jobs = currentData
                )
            )
        }
    }

    private fun isInputtedTreesValid(job: Job, staff: Staff, row: Row, trees: Int): Boolean {
        if (trees > row.totalTrees || trees > row.availableTress) {
            return false
        }

        val otherStaff = job.staffs.filter { it != staff }
        var assignedTreesForRow = 0
        otherStaff.forEach { staff ->
            staff.availableRows.find { it == row && it.isSelected }?.let { itemRow ->
                assignedTreesForRow += itemRow.trees
            }
        }

        return trees <= (row.availableTress - assignedTreesForRow)
    }

    fun updateSingleStaffRate(job: Job, staff: Staff, rate: Double) {
        val currentData: List<Job> = _state.value?.jobs!!
        currentData.find { jobItem ->
            jobItem == job
        }?.let { job ->
            job.staffs.find { staffItem ->
                staffItem == staff
            }?.let { staff ->
                staff.rate = rate
            }
        }
    }

    fun addMaxTreesForStaff(job: Job) {
        val currentData: List<Job> = _state.value?.jobs!!
        currentData.find { jobItem ->
            jobItem == job
        }?.let { job ->
            val listRows = mutableListOf<Row>()
            job.staffs.forEach { staffItem ->
                listRows.addAll(staffItem.availableRows)
            }
            val listRefinedRows = listRows.distinctBy {
                it.rowNumber
            }

            for (row in listRefinedRows) {
                val activeStaff =
                    job.staffs.count { it.availableRows.find { rowItem -> rowItem == row && rowItem.isSelected } != null }
                if (activeStaff == 0) continue
                val averageTrees = row.availableTress / activeStaff
                var odd = row.availableTress % activeStaff
                job.staffs.forEach { staff ->
                    staff.availableRows.find { it == row && it.isSelected }?.let { row ->
                        row.trees = averageTrees + odd
                        odd = 0
                    }
                }
            }
        }

        _state.postValue(TimesheetState(jobs = currentData))
    }
}
