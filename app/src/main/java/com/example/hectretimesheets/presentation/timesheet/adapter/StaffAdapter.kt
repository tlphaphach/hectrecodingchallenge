package com.example.hectretimesheets.presentation.timesheet.adapter

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.hectretimesheets.common.GridSpacingDecoration
import com.example.hectretimesheets.databinding.ItemStaffBinding
import com.example.hectretimesheets.domain.model.Row
import com.example.hectretimesheets.domain.model.Staff

class StaffAdapter() : ListAdapter<Staff, StaffAdapter.StaffVH>(StaffDiffCallback()) {

    inner class StaffVH(private val binding: ItemStaffBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val rowAdapter = RowAdapter()
        private val treesPerRowAdapter = TreesPerRowAdapter()
        private val itemDecoration = GridSpacingDecoration(7, 8)

        fun bindData(
            data: Staff,
            isLastPosition: Boolean
        ) {
            binding.vDivider.visibility = if (isLastPosition) View.GONE else View.VISIBLE
            // Set staff avatar
            binding.tvStaffAvatar.text = data.staffAvatar.first
            binding.tvStaffAvatar.backgroundTintList =
                ColorStateList.valueOf(data.staffAvatar.second)
            // Set staff name
            binding.tvStaffName.text = data.staffName

            // Set orchard
            binding.tvOrchard.text = data.orchard

            // Set block
            binding.tvBlock.text = data.block

            // Set wage description
            binding.tvWageDescription.text = "$jobName will be paid by wages in this timesheet."

            // Set rows adapter
            binding.rvAvailableRows.adapter = rowAdapter
            (binding.rvAvailableRows.itemAnimator as SimpleItemAnimator).supportsChangeAnimations =
                false
            binding.rvAvailableRows.removeItemDecoration(itemDecoration)
            binding.rvAvailableRows.addItemDecoration(itemDecoration)
            rowAdapter.submitList(data.availableRows)
            rowAdapter.setonRowClickedListener { isSelected, row ->
                onRowClickedListener?.let { it(data, isSelected, row) }
            }

            // Set trees per row adapter
            binding.rvTreesPerRow.adapter = treesPerRowAdapter
            (binding.rvTreesPerRow.itemAnimator as SimpleItemAnimator).supportsChangeAnimations =
                false
            treesPerRowAdapter.submitList(data.availableRows.filter { it.isSelected })
            treesPerRowAdapter.setOnTreesPerRowChangedListener { trees, row ->
                onTreesPerRowChangedListener?.let { it(data, row, trees) }
            }

            // Handle on PIECE RATE clicked
            binding.btnPieceRate.setOnClickListener {
                binding.tvWageDescription.visibility = View.GONE
                binding.gvPieceRateContent.visibility = View.VISIBLE
                binding.btnWages.isEnabled = true
                // Disable wage button to prevent the user taps on it again
                binding.btnPieceRate.isEnabled = false
            }

            // Handle on WAGE clicked
            binding.btnWages.setOnClickListener {
                binding.gvPieceRateContent.visibility = View.GONE
                binding.tvWageDescription.visibility = View.VISIBLE
                binding.btnPieceRate.isEnabled = true
                // Disable wage button to prevent the user taps on it again
                binding.btnWages.isEnabled = false
            }

            binding.edtRate.setText(data.rate.toString())
            binding.edtRate.doOnTextChanged { text, _, _, _ ->
                onStaffRateChangedListener?.let {
                    if (!text.isNullOrBlank()) {
                        it(data, text.toString().toDouble())
                    }
                }
            }
            binding.btnApplyToAll.setOnClickListener { _ ->
                val inputtedRate = binding.edtRate.text.toString().toDouble()
                onApplyToAllListener?.let { it -> it(inputtedRate) }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StaffVH {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemStaffBinding.inflate(inflater, parent, false)
        return StaffVH(binding)
    }

    override fun onBindViewHolder(holder: StaffVH, position: Int) {
        val data = getItem(position)
        val isLastPosition = position == itemCount - 1

        holder.bindData(data, isLastPosition)
    }

    private var onApplyToAllListener: ((rate: Double) -> Unit)? = null
    fun setOnApplyToAllListener(listener: (Double) -> Unit) {
        onApplyToAllListener = listener
    }

    private var onTreesPerRowChangedListener: ((staff: Staff, row: Row, trees: Int) -> Unit)? = null
    fun setOnTreesPerRowChangedListener(listener: (Staff, Row, Int) -> Unit) {
        onTreesPerRowChangedListener = listener
    }

    private var onRowClickedListener: ((staff: Staff, isSelected: Boolean, row: Row) -> Unit)? =
        null

    fun setonRowClickedListener(listener: (Staff, Boolean, Row) -> Unit) {
        onRowClickedListener = listener
    }

    private var jobName: String? = null
    fun setJobName(jobName: String) {
        this.jobName = jobName
    }

    private var onStaffRateChangedListener: ((staff: Staff, rate: Double) -> Unit)? = null
    fun setOnStaffRateChangedListener(listener: (Staff, Double) -> Unit) {
        onStaffRateChangedListener = listener
    }
}

class StaffDiffCallback : DiffUtil.ItemCallback<Staff>() {
    override fun areItemsTheSame(oldItem: Staff, newItem: Staff): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Staff, newItem: Staff): Boolean {
        return oldItem.isContentChanged(newItem)
    }
}