package com.example.hectretimesheets.presentation.timesheet.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.hectretimesheets.common.TextGenerator
import com.example.hectretimesheets.databinding.ItemTreesPerRowBinding
import com.example.hectretimesheets.domain.model.Row

class TreesPerRowAdapter :
    ListAdapter<Row, TreesPerRowAdapter.TreesPerRowVH>(TreesPerRowDiffCallback()) {

    inner class TreesPerRowVH(private val binding: ItemTreesPerRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(data: Row) {
            // Create row's title
            binding.tvRowTitle.text = "Tree for row ${data.rowNumber}"

            // Setup input field
            binding.edtTrees.setText(
                TextGenerator.generateDisplayTreePerRow(
                    data.trees.toString(),
                    data.totalTrees.toString()
                )
            )
            binding.edtTrees.setOnClickListener { editText ->
                handleSelectionPosition(editText as EditText)
            }

            binding.edtTrees.doOnTextChanged { text, _, before, _ ->
                val suffix = "/${data.totalTrees}"
                val inputtedValue = text.toString().removeSuffix(suffix)

                if (inputtedValue.contains('/')) {
                    val newInputtedValue = inputtedValue.substringAfter(suffix)
                    binding.edtTrees.setText(
                        TextGenerator.generateDisplayTreePerRow(
                            newInputtedValue,
                            data.totalTrees.toString()
                        )
                    )
                    handleSelectionPosition(binding.edtTrees)
                    return@doOnTextChanged
                }

                onTreesPerRowChanged?.let {
                    val trees = try {
                        text.toString().removeSuffix(suffix).toInt()
                    } catch (e: Exception) {
                        binding.edtTrees.setText(
                            TextGenerator.generateDisplayTreePerRow(
                                "0",
                                data.totalTrees.toString()
                            )
                        )
                        "0".toInt()
                    }
                    it(trees, data)
                }
            }

            // Set co-worker information
            if (data.isHavingCoWorker) {
                val strCoWorkers: String = TextGenerator.generateStrCoWorker(data.coWorkers)
                binding.tvCoWorkers.visibility = View.VISIBLE
                binding.tvCoWorkers.text = strCoWorkers
            } else {
                binding.tvCoWorkers.visibility = View.GONE
            }
        }

        private fun handleSelectionPosition(editText: EditText) {
            val selectionPosition = editText.selectionEnd
            val suffixStartPosition = editText.text.toString().indexOf('/')
            if (selectionPosition > suffixStartPosition) {
                editText.setSelection(suffixStartPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TreesPerRowVH {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemTreesPerRowBinding.inflate(inflater, parent, false)
        return TreesPerRowVH(binding)
    }

    override fun onBindViewHolder(holder: TreesPerRowVH, position: Int) {
        val data = getItem(position)
        holder.bindData(data)
    }

    private var onTreesPerRowChanged: ((inputtedTress: Int, row: Row) -> Unit)? = null
    fun setOnTreesPerRowChangedListener(listener: (Int, Row) -> Unit) {
        onTreesPerRowChanged = listener
    }
}

class TreesPerRowDiffCallback : DiffUtil.ItemCallback<Row>() {
    override fun areItemsTheSame(oldItem: Row, newItem: Row): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Row, newItem: Row): Boolean {
        return oldItem.trees == newItem.trees
    }
}