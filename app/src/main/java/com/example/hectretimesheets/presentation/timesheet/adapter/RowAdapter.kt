package com.example.hectretimesheets.presentation.timesheet.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.hectretimesheets.R
import com.example.hectretimesheets.databinding.ItemRowMultiStateBinding
import com.example.hectretimesheets.domain.model.Row

class RowAdapter : ListAdapter<Row, RowAdapter.MultiStateRowVH>(RowDiffCallback()) {

    inner class MultiStateRowVH(
        private val binding: ItemRowMultiStateBinding,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindData(data: Row, onClicked: (isSelected: Boolean, row: Row) -> Unit) {
            updateViewHolderStyle(data.isSelected, data.isHavingCoWorker)
            binding.tvBlockNumber.text = data.rowNumber.toString()

            binding.root.setOnClickListener {
                updateViewHolderStyle(!data.isSelected, data.isHavingCoWorker)
                onClicked(!data.isSelected, data)
            }
        }

        private fun updateViewHolderStyle(
            isSelected: Boolean,
            isHavingCoWorker: Boolean
        ) {
            binding.root.background = ResourcesCompat.getDrawable(
                context.resources,
                if (isSelected) {
                    R.drawable.bg_row_selected
                } else {
                    R.drawable.bg_row_unselected
                },
                null
            )
            binding.tvBlockNumber.setTextColor(if (isSelected) Color.WHITE else Color.BLACK)
            binding.vDot.visibility =
                if (isHavingCoWorker && isSelected) View.VISIBLE else View.GONE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MultiStateRowVH {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRowMultiStateBinding.inflate(inflater, parent, false)
        return MultiStateRowVH(binding, parent.context)
    }

    override fun onBindViewHolder(holder: MultiStateRowVH, position: Int) {
        val data = getItem(position)
        holder.bindData(data) { isSelected, row ->
            onRowClickedListener?.let { it(isSelected, row) }
        }
    }

    private var onRowClickedListener: ((isSelected: Boolean, row: Row) -> Unit)? = null
    fun setonRowClickedListener(listener: (Boolean, Row) -> Unit) {
        onRowClickedListener = listener
    }
}

class RowDiffCallback : DiffUtil.ItemCallback<Row>() {
    override fun areItemsTheSame(oldItem: Row, newItem: Row): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Row, newItem: Row): Boolean {
        return oldItem.isSelected == newItem.isSelected
                && oldItem.isHavingCoWorker == newItem.isHavingCoWorker
    }
}