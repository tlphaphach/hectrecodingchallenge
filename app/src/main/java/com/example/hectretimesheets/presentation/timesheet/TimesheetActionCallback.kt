package com.example.hectretimesheets.presentation.timesheet

import com.example.hectretimesheets.domain.model.Job
import com.example.hectretimesheets.domain.model.Row
import com.example.hectretimesheets.domain.model.Staff

interface TimesheetActionCallback {
    fun onAddMaxTreesClicked(job: Job)
    fun onApplyToAllClicked(job: Job, rate: Double)
    fun onTreesPerRowChanged(job: Job, staff: Staff, row: Row, trees: Int)
    fun onRowClicked(job: Job, staff: Staff, row: Row, isSelected: Boolean)
    fun onStaffRateChanged(job: Job, staff: Staff, rate: Double)
}