package com.example.hectretimesheets.common

import android.graphics.Color

object ColorGenerator {
    private val colorList: IntArray = intArrayOf(
        Color.parseColor("#A96666"),
        Color.parseColor("#6695AA"),
        Color.parseColor("#AA8666"),
    )

    fun getRandomIntColor(): Int {
        return colorList.random()
    }
}