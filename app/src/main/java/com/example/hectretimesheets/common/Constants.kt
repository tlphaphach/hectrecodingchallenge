package com.example.hectretimesheets.common

object Constants {
    const val BASE_URL = "https://api.hectretimeshee.com/"

    const val PARAM_TIMESHEET_DATA = "timesheetData"
}