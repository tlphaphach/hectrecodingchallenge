package com.example.hectretimesheets.common

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import com.example.hectretimesheets.domain.model.CoWorker


object TextGenerator {
    fun generateStrCoWorker(coWorkers: List<CoWorker>): String {
        var result = ""

        if (!coWorkers.isNullOrEmpty()) {
            coWorkers.forEach {
                result += "${it.name} (${it.trees}), "
            }
            if (result.isNotBlank()) {
                result = result.dropLast(2)
            }
        }

        return result
    }

    fun generateDisplayTreePerRow(input: String, suffix: String): Spannable {
        val wholeText = "$input/$suffix"
        val spannable = SpannableString(wholeText)
        spannable.setSpan(
//            ForegroundColorSpan(Resources.getSystem().getColor(R.color.color_grey, null)),
            ForegroundColorSpan(Color.parseColor("#ACCBCA")),
            wholeText.indexOf('/'),
            wholeText.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return spannable
    }

    fun generateAvatarTextByName(name: String): String {
        return name.first().uppercase()
    }
}